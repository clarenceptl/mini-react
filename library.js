

function type_check(variable, conf) {
  if (!type_check_v2(variable, conf)) return false;
  if (conf.properties) {
    return Object.keys(conf.properties).reduce(
      (acc, value) =>
        acc &&
        variable[value] !== undefined &&
        type_check(variable[value], conf.properties[value]),
      true
    );
  }
  return true;
}
function type_check_v2(variable, conf) {
  if (conf.type) {
    if (type_check_v1(variable, conf.type) === false) return false;
  }
  if (conf.value) {
    if (JSON.stringify(conf.value) !== JSON.stringify(variable)) return false;
  }
  if (conf.enum) {
    if (
      !(conf.enum instanceof Array) ||
      !conf.enum.reduce(
        (acc, value) => acc || type_check_v2(variable, { value }),
        false
      )
    )
      return false;
  }
  return true;
}

function type_check_v1(variable, type) {
  switch (typeof variable) {
    case "number":
    case "string":
    case "array":
    case "boolean":
    case "undefined":
    case "null":
    case "function":
      return typeof variable === type;

    case "object":
      switch (type) {
        case "null":
          return variable === null;
        case "array":
          return Array.isArray(variable);
        case "object":
          return variable !== null && !Array.isArray(variable);

        default:
          return false;
      }

    default:
      return false;
  }
}

function prop_access(obj, path) {
  if(typeof path !=="string"||!path) return obj;
  let param_table = path.trim().split('.');
  let loop_table;
  for (let element of param_table) {
    if(loop_table){
      if(Object.keys(loop_table).includes(element)){
        loop_table = loop_table[element];
      }else{
        console.log(path+ ' not exist.');
        return null
      }
    }else{
      if(obj&&Object.keys(obj).includes(element)){
        loop_table = obj[element];
      }else{
        console.log(path+ ' not exist.');
        return null
      }
    }
   
  }
  return loop_table
}

//link component
function link(label, path) {
  return {
    type: "a",
    attributes: {
      href: path,
      onClick: (e) => {
        e.preventDefault();
        history.pushState({ title: label }, label, path);
        root.dispatchEvent(new Event("rerender"));
      },
    },
    children: [label],
  };
}

export { type_check, prop_access, link };
