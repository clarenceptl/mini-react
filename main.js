import { type_check, prop_access, link } from "./library.js";

class MiniReact {
  constructor() {
    this.props = {};
  }

  shouldUpdate(newProps) {
    return newProps === this.props ? false : true;
  }
  display(newProps) {
    if (type_check(newProps,{type: 'object'})&&this.shouldUpdate(newProps)) {
      this.props = newProps;
      root.replaceChild(this.render(), root.firstChild);
    }
  }

  render(struct) {
    return generateStructure(struct);
  }
}

String.prototype.interpolate = function (obj) {
  if (this) {
    let [variable] = this.match(/\{\{(.*?)\}\}/g);
    variable = variable.replace("{{", "").replace("}}", "").trim();
    return prop_access(obj, variable);
  }
};

class Page1 extends MiniReact {
  constructor() {
    super();
    this.props = {number: 0}
  }

  render() {
    let compteur = "Compteur : {{ number }}"
    return super.render({
      type: "div",
      attributes: {
        style: "text-align: center;",
        class: "container",
      },
      children: [
        {
          type: "h1",
          children: "Mini react",
          attributes: {
            class: "border border-info rounded bg-info mb-4",
            style: "text-align:center",
          },
        },
        {
          type: "nav",
          children: [
            "Page 1",
            link("Page 2", "/page2"),
            link("Page 3", "/page3"),
          ],
          attributes: {
            style: "justify-content: space-around;",
            class: "d-flex mt-4 border border-info rounded bg-info",
          },
        },
        {
          type: "div",
          attributes: {
            class: "container"
          },
          children: [{
            type: "h1",
            children: 'Bienvenue sur le mini react',
            class: "mt-5"
          }],
        },
        {
          type: "div",
          attributes: {
            class: "compteur"
          },
          children: [
            {
              type: "p",
              children: "Compteur : " + compteur.interpolate(this.props),
            },
            {
              type: "button",
              attributes:{
                class: "btn btn-primary btn-sm",
                onClick: (e) => {
                  super.display({number: this.props.number+1});
                }
              },
              children: 'click'
            }
          ],
        }
      ],
    });
  }
}

class Page2 extends MiniReact {
  constructor() {
    super();
  }
  previewFile() {
    const preview = document.querySelector("img");
    const file = document.querySelector("input[type=file]").files[0];
    const reader = new FileReader();

    reader.addEventListener(
      "load",
      function () {
        // convert image file to base64 string
        preview.src = reader.result;
      },
      false
    );

    if (file) {
      reader.readAsDataURL(file);
    }
  }
  pkmDisplay() {
    fetch(
      "https://pokeapi.co/api/v2/pokemon/" +
        document.getElementById("pkm-name").value.toLowerCase() +
        "/"
    )
      .then((response) => response.json())
      .then((output) => {
        var pkmSprite = output["sprites"]["front_default"];
        var pkmName = output["name"];
        document.getElementById("pkm-sprite").setAttribute("src", pkmSprite);
        document.getElementById("pkm-name-display").textContent = pkmName.toUpperCase();
      })
      .catch((error) => console.log("Erreur : " + error));
  }
  render() {
    return super.render({
      type: "div",
      attributes: {
        style: "text-align: center;",
        class: "container",
      },
      children: [
        {
          type: "h1",
          children: "Mini react",
          attributes: {
            class: "border border-info rounded bg-info mb-4",
            style: "text-align:center",
          },
        },
        {
          type: "nav",
          children: [
            link("Page 1", "/page1"),
            "Page 2",
            link("Page 3", "/page3"),
          ],
          attributes: {
            style: "justify-content: space-around;",
            class: "d-flex mt-4 border border-info rounded bg-info",
          },
        },
        {
          type: "input",
          attributes: {
            type: "file",
            onChange: this.previewFile,
          },
        },
        {
          type: "img",
          attributes: {
            src: "",
            height: "600",
          },
        },
        {
          type: "h2",
          children: ["Entrée le nom d'un pokémon"],
        },
        {
          type: "input",
          attributes: {
            type: "text",
            id: "pkm-name",
            onChange: this.pkmDisplay,
          },
        },
        {
          type: "br",
        },
        {
          type: "img",
          attributes: {
            id: "pkm-sprite",
          },
        },
        {
          type: "h4",
          attributes: {
            id: "pkm-name-display",
          },
        },
        /*
        {
          type: "output",
          attributes: {
            id: "log-div"
          }
        }*/
        ,
      ],
    });
  }
}

class Page3 extends MiniReact {
  constructor() {
    super();
    this.props = { item: "test" };
  }

  render() {
    const test = "hello {{ item }}";
    // console.log(test.interpolate(this.props),'jjhjh')
    return super.render({
      type: "div",
      attributes: {
        class: "container",
        style: "text-align: center;",
      },
      children: [
        {
          type: "h1",
          children: "Mini react",
          attributes: {
            class: "border border-info rounded bg-info mb-4",
            placeholder: "Write something",
            style: "margin:auto;",
          },
        },
        {
          type: "nav",
          children: [
            link("Page 1", "/page1"),
            link("Page 2", "/page2"),
            "Page 3",
          ],
          attributes: {
            style: "justify-content: space-around;",
            class: "d-flex mt-4 border border-info rounded bg-info",
          },
        },
        {
          type: "input",
          attributes: {
            id: "changeme",
            class: "form-control mb-2",
            placeholder: "Write something",
          },
        },
        {
          type: "button",
          children: "Click me",
          attributes: {
            style: "text-align:left;",
            class: "btn btn-info",
            onClick: (e) => {
              super.display({
                item: document.getElementById("changeme").value,
              });
            },
          },
        },
        {
          type: "h2",
          children: "Hello " + test.interpolate(this.props),
          attributes: {
            style: "text-align:left;",
          },
        },
      ],
    });
  }
}

function generatePage() {
  document.title = history?.state?.title;
  const currentPath = window.location.pathname;
  let elem;
  switch (currentPath) {
    case "/page1":
      elem = new Page1();
      break;
    case "/page2":
      elem = new Page2();
      break;
    case "/page3":
      elem = new Page3();
      break;
    default:
      elem = new Page1();
      break;
  }
  if (root.firstChild) {
    root.replaceChild(elem.render(), root.firstChild);
  } else {
    root.appendChild(elem.render());
  }
}

root.addEventListener("rerender", generatePage);

window.onpopstate = () => root.dispatchEvent(new Event("rerender"));
//root.appendChild(Page2());

const generateStructure = (structure) => {
  const node = document.createElement(structure.type);
  if (structure.attributes) {
    for (let attName in structure.attributes) {
      if (/on([A-Z].*)/.test(attName)) {
        const eventName = attName.match(/on([A-Z].*)/)[1].toLowerCase();
        node.addEventListener(eventName, structure.attributes[attName]);
      } else {
        node.setAttribute(attName, structure.attributes[attName]);
      }
    }
  }
  if (structure.dataset) {
    for (let attName in structure.dataset) {
      node.dataset[attName] = structure.dataset[attName];
    }
  }
  if (structure.children)
    for (let child of structure.children) {
      if (child === undefined) continue;
      if (typeof child === "string") {
        node.appendChild(document.createTextNode(child));
      } else {
        node.appendChild(generateStructure(child));
      }
    }
  structure.node = node;

  return node;
};
root.dispatchEvent(new Event("rerender"));
